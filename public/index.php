<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=2">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Tablo</title>

	<!--
  <link rel="stylesheet" href="lib/css/bootstrap.min.css">
  <link rel="stylesheet" href="lib/css/mdb.min.css">
  <link rel="stylesheet" href="lib/css/fontawesome-5.11.2/css/all.css">
-->
	<link rel="stylesheet" href="lib/css/compiled-4.9.0.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<div class="container">
		<?php
    include_once 'tablo.php';
?>
	</div>

	<!--
  <script src="lib/js/jq331.min.js"></script>
  <script src="lib/js/popper.min.js"></script>
  <script src="lib/js/bootstrap.min.js"></script>
  <script src="lib/js/mdb.min.js"></script>
-->
	<script src="lib/js/compiled-4.9.0.min.js"></script>
</body>

</html>